package com.example.spaceapps1.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;


public class GameTextActivity extends ActionBarActivity {

    TextGameMode game;
    GameQuestion current_question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gametextactivity);
        game = new TextGameMode();
        Intent intentd = getIntent();
        int level = intentd.getIntExtra(MainActivity.DIFICULTY_LEVEL, GameModeBase.EASY_LEVEL);
        if (level == GameModeBase.EASY_LEVEL) game.setEasyLevel();
        else if( level == GameModeBase.MEDIUM_LEVEL) game.setMediumLevel();
        else if (level == GameModeBase.HARD_LEVEL) game.setHardLevel();
        game.update(new GameModeBase.UpdateObserver() {
            @Override
            public void onUpdate(boolean error) {
                processNextQuestion();
            }
        });
    }

    private void updateLivesAndScore() {
        TextView lives_view = (TextView)findViewById(R.id.lives2);
        TextView score_view = (TextView)findViewById(R.id.points2);
        lives_view.setText(String.valueOf(game.getLives()));
        score_view.setText(String.valueOf(game.getScore()));
    }


    private void processNextQuestion() {
        updateLivesAndScore();
        try {
            ImageView img = (ImageView) findViewById(R.id.imageView);
            img.setImageResource(R.drawable.loading);
            current_question = game.getNextQuestion();
            String image = current_question.getImageURL(0);
            String msg1 = current_question.getMsg(0);
            String msg2 = current_question.getMsg(1);
            String msg3 = current_question.getMsg(2);
            String msg4 = current_question.getMsg(3);

            RadioButton rb1=(RadioButton)findViewById(R.id.opt1);
            RadioButton rb2=(RadioButton)findViewById(R.id.opt2);
            RadioButton rb3=(RadioButton)findViewById(R.id.opt3);
            RadioButton rb4=(RadioButton)findViewById(R.id.opt4);

            rb1.setText(msg1);
            rb2.setText(msg2);
            rb3.setText(msg3);
            rb4.setText(msg4);

            new DownloadImageTask((ImageView) findViewById(R.id.imageView))
                    .execute(image);
        }  catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gametextactivity, menu);
        return true;
    }

    private int getAnswer() {
        RadioGroup r=(RadioGroup)findViewById(R.id.radioGroup2);
        int id = r.getCheckedRadioButtonId();
        View rb = r.findViewById(id);
        int rbid = r.indexOfChild(rb);
        Log.d("game image activity", String.valueOf(rbid));
        return rbid;
    }


    public void sendResponse(View view) {
        int answer = getAnswer();
        if (game.solve(answer)) {
            processNextQuestion();
        }
        else updateLivesAndScore();
    }
}
