package com.example.spaceapps1.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;


public class GameImageActivity extends ActionBarActivity {

    ImageGameMode game;
    GameQuestion current_question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameimageactivity);
        game = new ImageGameMode();
        Intent intent = getIntent();
        int level = intent.getIntExtra(MainActivity.DIFICULTY_LEVEL, GameModeBase.EASY_LEVEL);
        if (level == GameModeBase.EASY_LEVEL) game.setEasyLevel();
        else if (level == GameModeBase.MEDIUM_LEVEL) game.setMediumLevel();
        else if (level == GameModeBase.HARD_LEVEL) game.setHardLevel();
        game.update(new GameModeBase.UpdateObserver() {
            @Override
            public void onUpdate(boolean error) {
                processNextQuestion();
            }
        });
    }

    private void setDefaultImage(View v) {
        ImageView img = (ImageView) v;
        img.setImageResource(R.drawable.loading);
    }

    private void updateLivesAndScore() {
        TextView lives_view = (TextView)findViewById(R.id.lives);
        TextView score_view = (TextView)findViewById(R.id.points);
        lives_view.setText(String.valueOf(game.getLives()));
        score_view.setText(String.valueOf(game.getScore()));
    }

    private void processNextQuestion() {
        updateLivesAndScore();
        try {
            setDefaultImage(findViewById(R.id.imageView1));
            setDefaultImage(findViewById(R.id.imageView2));
            setDefaultImage(findViewById(R.id.imageView3));
            setDefaultImage(findViewById(R.id.imageView4));
            current_question = game.getNextQuestion();
            String image1 = current_question.getImageURL(0);
            String image2 = current_question.getImageURL(1);
            String image3 = current_question.getImageURL(2);
            String image4 = current_question.getImageURL(3);
            String msg = current_question.getMsg(0);
            // TODO: set the views with the correct content
            new DownloadImageTask((ImageView) findViewById(R.id.imageView1))
                    .execute(image1);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView2))
                    .execute(image2);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView3))
                    .execute(image3);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView4))
                    .execute(image4);

            TextView t=(TextView)findViewById(R.id.textView3);
            t.setText(msg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gameimageactivity, menu);
        return true;
    }


    private void sendResponse(int i) {
        if (game.solve(i)) processNextQuestion();
        else updateLivesAndScore();
    }

    public void sendResponse1(View view) {
        sendResponse(0);
    }

    public void sendResponse2(View view) {
        sendResponse(1);
    }

    public void sendResponse3(View view) {
        sendResponse(2);
    }

    public void sendResponse4(View view) {
        sendResponse(3);
    }

}
