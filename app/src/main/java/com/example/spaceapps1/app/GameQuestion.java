package com.example.spaceapps1.app;
import java.net.URL;

/**
 * Created by pakozm on 12/04/14.
 */
public class GameQuestion {
    private String image_URLs[];
    private String msgs[];
    private String[] hints;
    private int current_hint;

    GameQuestion(String image_URLs_[],
                 String msgs_[],
                 String[] hints_) {
        image_URLs = image_URLs_;
        msgs = msgs_;
        hints = hints_;
        current_hint = 0;
    }
    public String getImageURL(int n) {
        if (n <= image_URLs.length) {
            return image_URLs[n];
        }
        else {
            return null;
        }
    }
    public String getMsg(int n) { return msgs[n]; }
    public boolean hasNextHint() {
        return current_hint < hints.length;
    }
    public String getNextHint() {
        if (current_hint < hints.length) {
            return hints[current_hint];
        }
        else {
            return null;
        }
    }

}
