package com.example.spaceapps1.app;

import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pakozm on 12/04/14.
 */
public class ImageGameMode extends GameModeBase {

    ImageGameMode() {
        super();
    }

    @Override public GameQuestion getNextQuestion() throws JSONException {
        ParseObject correct_answer = getAndRemoveObject();
        ParseObject[] other_answers = getNObjects(3);
        if (correct_answer == null || other_answers == null) return null;
        String image_URLs[] = new String[4];
        for (int i=0; i<3; ++i) {
            image_URLs[i] = other_answers[i].getString("URL");
        }
        image_URLs[3] = correct_answer.getString("URL");
        /*
        JSONArray parse_hints = correct_answer.getJSONArray("hints");
        String[] hints = new String[parse_hints.length()];
        for (int i=0; i<hints.length; ++i) {
            hints[i] = parse_hints.get(i).toString();
        }
        */
        String hints[] = new String[0];
        String where = correct_answer.getString("where");
        String what = correct_answer.getString("what");
        String[] msg = new String[1];
        if (Math.random() > 0.5)
            msg[0] = what;
        else msg[0] = where;
        int which = (int)(Math.random()*4);
        String aux = image_URLs[which];
        image_URLs[which] = image_URLs[3];
        image_URLs[3] = aux;
        setCorrectAnswer(which);
        GameQuestion question = new GameQuestion(image_URLs, msg, hints);
        return question;
    }

}
