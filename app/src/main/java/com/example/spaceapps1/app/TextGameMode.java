package com.example.spaceapps1.app;

import android.util.Log;

import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pakozm on 12/04/14.
 */
public class TextGameMode extends GameModeBase {

    TextGameMode() {
        super();
    }

    @Override public GameQuestion getNextQuestion() throws JSONException {
        ParseObject correct_answer = getAndRemoveObject();
        ParseObject[] other_answers = getNObjects(3);
        if (correct_answer == null || other_answers == null) {
            Log.d("text game mode", "No available data");
            return null;
        }
        String where[] = new String[4];
        String what[] = new String[4];
        for (int i=0; i<3; ++i) {
            what[i] = other_answers[i].getString("what");
            where[i] = other_answers[i].getString("where");
        }
        what[3] = correct_answer.getString("what");
        where[3] = correct_answer.getString("where");
        /*
        JSONArray parse_hints = correct_answer.getJSONArray("hints");
        String[] hints = new String[parse_hints.length()];
        for (int i=0; i<hints.length; ++i) {
            hints[i] = parse_hints.get(i).toString();
        }
        */
        String[] hints = new String[0];
        String image_url[] = new String[1];
        image_url[0] = correct_answer.getString("URL");
        String msg[];
        if (Math.random() < 0.5)
            msg = where;
        else msg = what;
        int which = (int)(Math.random()*4);
        String aux_msg = msg[which];
        msg[which] = msg[3];
        msg[3] = aux_msg;
        setCorrectAnswer(which);
        GameQuestion question = new GameQuestion(image_url, msg, hints);
        return question;
    }

}
