package com.example.spaceapps1.app;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class RankingActivity extends ActionBarActivity {

    private RankingModel ranking;
    private ArrayAdapter<RankingModel.RankingRow> ranking_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rankingactivity);
        final Context parent = this;
        ranking = new RankingModel();
        ranking.update(new RankingModel.UpdateObserver() {
            @Override
            public void onUpdate(boolean error) {
                RankingModel.RankingRow[] full_ranking = ranking.getFullRanking();
                ListView list = (ListView) findViewById(R.id.listRanking2);
                ranking_adapter = new ArrayAdapter<RankingModel.RankingRow>(parent,
                        R.layout.ranking_list_layout,
                        full_ranking);
                list.setAdapter(ranking_adapter);
            }
        });
    }




}
