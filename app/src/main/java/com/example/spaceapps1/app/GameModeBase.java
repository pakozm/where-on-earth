package com.example.spaceapps1.app;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by pakozm on 12/04/14.
 */
public abstract class GameModeBase {
    public static final String DEFAULT_URL="http://barrapunto.com/images/topics/topicdatabases.gif";
    public static final String DEFAULT_WHERE="WHERE";
    public static final String DEFAULT_WHAT="WHAT";
    public static final String DEFAULT_HINT="HINT";

    // Observer classes, for live up and difficulty changes
    public interface DifficultyLevelObserver {
        public void onDifficultyLevelChange(int difficulty_level);
    }
    public interface LiveUpObserver {
        public void onLiveUp(int lives);
    }
    public interface UpdateObserver {
        public void onUpdate(boolean error);
    }
    private class DefaultDifficultyLevelObserver implements DifficultyLevelObserver {
        public void onDifficultyLevelChange(int difficulty_level) { }
    }
    private class DefaultLiveUpObserver implements LiveUpObserver {
        public void onLiveUp(int lives) { }
    }
    // Difficulty level constants
    public static final int EASY_LEVEL=0;
    public static final int MEDIUM_LEVEL=1;
    public static final int HARD_LEVEL=2;
    public static final int LEVEL_POINTS[] = { 10, 20, 30 };
    //
    private static final int INITIAL_LIVES=3;
    private static final int LIVE_UP_THRESHOLD=100;
    private static final int BUY_QUESTION_POINTS=200; // price to buy a question
    //
    private static final int MEDIUM_THRESHOLD=20;
    private static final int HARD_THRESHOLD=50;
    //
    private static final int INITIAL_POINTS=0;
    // Properties
    private int difficulty_level;
    private int correct_answer;
    private int score;
    private int lives;
    private int N;
    private boolean finished;
    private List<ParseObject> easy_images;
    private List<ParseObject> medium_images;
    private List<ParseObject> hard_images;

    /////////////////////////////////// PRIVATE API ////////////////////////////////////////////

    // Set of difficulty levels
    public void setHardLevel() {
        difficulty_level = HARD_LEVEL;
    }
    public void setMediumLevel() {
        difficulty_level = MEDIUM_LEVEL;
    }
    public void setEasyLevel() {
        difficulty_level = EASY_LEVEL;
    }

    // Checks the number of questions solved, and changes the difficulty level
    private void updateGameStatus(DifficultyLevelObserver level_observer,
                                  LiveUpObserver live_up_observer) {
        boolean difficulty_level_changed = false;
        if (difficulty_level == EASY_LEVEL) {
            if (N >= MEDIUM_THRESHOLD || easy_images.size() < 4) {
                setMediumLevel();
                difficulty_level_changed = true;
            }
        }
        if (difficulty_level == MEDIUM_LEVEL) {
            if (N >= HARD_THRESHOLD || medium_images.size() < 4) {
                setHardLevel();
                difficulty_level_changed = true;
            }
        }
        if (difficulty_level == HARD_LEVEL) {
            if (hard_images.size() < 4) finished = true;
        }
        if (difficulty_level_changed) level_observer.onDifficultyLevelChange(difficulty_level);
        if (score > LIVE_UP_THRESHOLD) {
            ++lives;
            live_up_observer.onLiveUp(lives);
        }
    }

    ////////////////////////////////// PROTECTED API ////////////////////////////////////////////

    // Sets the correct answer
    protected void setCorrectAnswer(int correct_answer_) {
        correct_answer = correct_answer_;
    }

    protected ParseObject getAndRemoveObject() {
        List<ParseObject> aux;
        if (difficulty_level == EASY_LEVEL) aux = easy_images;
        else if (difficulty_level == MEDIUM_LEVEL) aux = medium_images;
        else aux = hard_images;
        if (aux.isEmpty()) return null;
        ParseObject result = aux.get(0);
        aux.remove(0);
        return result;
    }
    protected ParseObject[] getNObjects(int n) {
        List<ParseObject> aux;
        if (difficulty_level == EASY_LEVEL) aux = easy_images;
        else if (difficulty_level == MEDIUM_LEVEL) aux = medium_images;
        else aux = hard_images;
        if (aux.size() < n) return null;
        ArrayList<Integer> positions = new ArrayList<Integer>();
        for (int i=0; i<aux.size(); ++i) positions.add(i);
        Collections.shuffle(positions);
        ParseObject[] result = new ParseObject[n];
        for (int i=0; i<n; ++i) {
            result[i] = aux.get(positions.get(i));
        }
        return result;
    }

    //////////////////////////////////// PUBLIC API ////////////////////////////////////////////

    public GameModeBase() {
        difficulty_level = EASY_LEVEL;
        correct_answer = -1;
        score = INITIAL_POINTS;
        lives = INITIAL_LIVES;
        N = 0;
        finished = false;
    }

    public int getScore() {
        return score;
    }

    public int getLives() {
        return lives;
    }

    public int getDifficultyLevel() {
        return difficulty_level;
    }

    public void update(final UpdateObserver observer) {
        easy_images = new ArrayList<ParseObject>();
        medium_images = new ArrayList<ParseObject>();
        hard_images = new ArrayList<ParseObject>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Images");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> images_list, ParseException e) {
                if (e == null) {
                    Log.d("images", "Retrieved " + images_list.size() + " images");
                    for (int i=0; i<images_list.size(); ++i) {
                        ParseObject obj = images_list.get(i);
                        int level = obj.getInt("level");
                        if (level == EASY_LEVEL)
                            easy_images.add(obj);
                        else if (level == MEDIUM_LEVEL)
                            medium_images.add(obj);
                        else if (level == HARD_LEVEL)
                            hard_images.add(obj);
                    }
                    Collections.shuffle(easy_images);
                    Collections.shuffle(medium_images);
                    Collections.shuffle(hard_images);
                    observer.onUpdate(false);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                    observer.onUpdate(true);
                }
            }
        });
    }

    public boolean isFinished() { return lives > 0 && !finished; }

    public int buyCorrectAnswer() {
        if (score >= BUY_QUESTION_POINTS) {
            score -= BUY_QUESTION_POINTS;
            return correct_answer;
        }
        else return -1;
    }

    // Process the answer of the user
    public boolean solve(int answer,
                         DifficultyLevelObserver level_observer,
                         LiveUpObserver live_up_observer) {
        if (answer == correct_answer) {
            score += LEVEL_POINTS[difficulty_level];
            ++N;
            updateGameStatus(level_observer, live_up_observer);
            return true;
        }
        else {
            --lives;
            return false;
        }
    }

    public boolean solve(int answer) {
        return solve(answer, new DefaultDifficultyLevelObserver(), new DefaultLiveUpObserver());
    }


    abstract GameQuestion getNextQuestion() throws JSONException;
}
