package com.example.spaceapps1.app;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.StringWriter;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;

/**
 * Created by pakozm on 12/04/14.
 */
public class RankingModel {
    private static final int TOP_RANKING_SIZE = 5;

    public interface UpdateObserver {
        public void onUpdate(boolean error);
    }

    public class RankingRow {
        int position;
        String username;
        int points;

        RankingRow(int position_, String username_, int points_) {
            position = position_;
            username = username_;
            points = points_;
        }

        @Override
        public String toString() {
            StringWriter result = new StringWriter();
            Formatter fmt = new Formatter(result);
            fmt.format("%4d  %10s  %6d", position, username, points);
            return result.toString();
        }
    }

    private static final int MAX_RANKING=100;
    private RankingRow[] ranking;
    int N;

    void update(final UpdateObserver observer) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Ranking");
        query.addDescendingOrder("score");
        query.setLimit(MAX_RANKING);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> ranking_list, ParseException e) {
                if (e == null) {
                    Log.d("ranking", "Retrieved " + ranking_list.size() + " users");
                    N = ranking_list.size();
                    ranking = new RankingRow[N];
                    for (int i=0; i<N; ++i) {
                        ParseObject obj = ranking_list.get(i);
                        ranking[i] = new RankingRow(i, obj.getString("username"), obj.getInt("score"));
                    }
                    observer.onUpdate(false);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                    observer.onUpdate(true);
                }
            }
        });
    }

    // Returns the whole ranking array
    public RankingRow[] getFullRanking() {
        return ranking;
    }

    // Returns the top raking
    public RankingRow[] getTopRanking() {
        RankingRow[] top_ranking = new RankingRow[Math.min(TOP_RANKING_SIZE, N)];
        for (int i=0; i<top_ranking.length; ++i) {
            top_ranking[i] = ranking[i];
            Log.d("ranking", top_ranking[i].toString());
        }
        return top_ranking;
    }
}
