package com.example.spaceapps1.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import com.parse.Parse;

public class MainActivity extends ActionBarActivity {

    public static final String DIFICULTY_LEVEL = "Dificulty_level";

    private RankingModel ranking;
    private ArrayAdapter<RankingModel.RankingRow> ranking_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Parse.initialize(this, "tHjIsROVNK5qsyOwdkjtR8qiU6Xk7zNut8T0K2vr", "clbW0rehsYDnHXc1xetjlbZ0FBxNEu7vOg9OAlqE");
        final Context parent = this;
        ranking = new RankingModel();
        ranking.update(new RankingModel.UpdateObserver() {
            @Override
            public void onUpdate(boolean error) {
                RankingModel.RankingRow[] top_ranking = ranking.getTopRanking();
                ListView list = (ListView) findViewById(R.id.listRanking);
                ranking_adapter = new ArrayAdapter<RankingModel.RankingRow>(parent,
                        R.layout.ranking_list_layout,
                        top_ranking);
                list.setAdapter(ranking_adapter);
            }
        });
        final Context p = this;
        ListView LR = (ListView) findViewById(R.id.listRanking);
        LR.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intr = new Intent(p, RankingActivity.class);
                p.startActivity(intr);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
    }

    private int getDifficultyLevel() {
        RadioGroup r=(RadioGroup)findViewById(R.id.radioGroup);
        int id = r.getCheckedRadioButtonId();
        View rb = r.findViewById(id);
        int rbid = r.indexOfChild(rb);
        Log.d("main activity", String.valueOf(rbid));
        return rbid;
    }

    public void onImageGameModeClick(View view) {
        Intent intent = new Intent(this, GameImageActivity.class);
        intent.putExtra(DIFICULTY_LEVEL, getDifficultyLevel());
        startActivity(intent);
    }
    public void onTextGameMode(View view){
        Intent intent2 = new Intent(this, GameTextActivity.class);
        intent2.putExtra(DIFICULTY_LEVEL, getDifficultyLevel());
        startActivity(intent2);

    }
}
